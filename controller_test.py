import random as random

class Controller(object):
	def __init__(self,up,down):
		self.up_key = up 
		self.down_key = down
		self.speed = 10
	def _user_input(self, coords, pressed):
		if self.up_key in pressed and self.down_key not in pressed:
			if coords[1]>=0:
				coords[1]-=self.speed
				coords[3]-=self.speed
				return coords
			else:
				return 'No change in location is made'
		if self.down_key in pressed and self.up_key not in pressed:
	 		if coords[3]<=400:	
				coords[1]+=self.speed
				coords[3]+=self.speed
				return coords
			else:
				return 'No change in location is made'
control1 = Controller('Up','Down')
position1 = [20,20,100,40]
newposition = control1._user_input(position1,'Up')
print newposition
newposition = control1._user_input(position1,'Down')
print newposition
position2 = [20,-1,100,40]
newposition = control1._user_input(position2,'Up')
print newposition
position3 = [20,20,100,401]
newposition = control1._user_input(position3,'Down')
print newposition
