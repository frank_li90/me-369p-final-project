from Tkinter import *
import math as math
import time
import random as random
import socket


class UI(Frame):
	winHeight = 400
	winWidth = 700
	def __init__(self,parent):
		Frame.__init__(self,parent)
		self.root = parent	
		self.root.title("PONG")
		self.canvas = Canvas(self.root,bg='black')
		self.canvas.configure(width = self.winWidth, height=self.winHeight)
		self.canvas.create_line(self.winWidth/2,0,self.winWidth/2,self.winHeight,fill = 'yellow')
		self.canvas.grid(row=0, column = 0,columnspan=10)
		self.pad1Obj = Paddle()
		self.pad2Obj = Paddle(687)
		self.ball = self.canvas.create_oval(self.ballObj.ball_position[0]+0, 
		 				self.ballObj.ball_position[1]+0, 2*self.ballObj.radius+self.ballObj.ball_position[0], 
		 				2*self.ballObj.radius+self.ballObj.ball_position[1], outline = 'white', fill = 'white', width = 1)
		self.pad1 = self.canvas.create_rectangle(0+self.pad1Obj.position[0], 0+self.pad1Obj.position[1], 
						self.pad1Obj.width+self.pad1Obj.position[0], 
	 					self.pad1Obj.height+self.pad1Obj.position[1], outline="white", fill="white")
	 	self.pad2 = self.canvas.create_rectangle(0+self.pad2Obj.position[0], 0+self.pad2Obj.position[1], 
						self.pad2Obj.width+self.pad2Obj.position[0], 
	 					self.pad2Obj.height+self.pad2Obj.position[1], outline="white", fill="white")
	 	self.control1 = Controller('w','s')
	 	self.control2 = Controller('Up','Down')
	 	self.buttons = set()
		self.root.bind_all('<KeyPress>', lambda event: self.buttons.add(event.keysym))
		self.root.bind_all('<KeyRelease>', lambda event: self.buttons.discard(event.keysym))		
	 	Button(self.root,text="START", command=self.start).grid(row=1,column=0)
	 	Button(self.root,text="QUIT", command=self.root.destroy).grid(row=2,column=0)
		Button(self.root,text="RESET", command=self.restart).grid(row=3,column=0)		
		# ball speed adjustment
		Label(self.root, text="Ball Speed Control").grid(row=1,column=1)
		self.speedLevel = Scale(self.root, from_=1, to=4,orient = HORIZONTAL,resolution=0.5,command = self.paceChange)
		self.speedLevel.grid(row=2,column=1)
		self.speedLevel.set(1)
		# pad speed adjustment
		Label(self.root, text="Paddle Speed Control").grid(row=1,column=2)
		self.padSpeedLevel = Scale(self.root, from_=5, to=15,orient = HORIZONTAL,resolution=1,command = self.padSpeedChange).grid(row=2,column=2)
		self.pad_shape = IntVar()
		self.PaddleShapeCheckBox = Checkbutton(self.root, text="Tricky Paddle", variable=self.pad_shape, command=self.changePaddleShape).grid(row=3,column=2)	
		self.gravitySwitch = IntVar()
		self.gravitySwitchCheckBox = Checkbutton(self.root, text="Gravity On", variable=self.gravitySwitch).grid(row=1,column=3)
		self.portalSwitch = IntVar()
		self.portalSwitchCheckBox = Checkbutton(self.root, text = 'Portals On', variable = self.portalSwitch).grid(row = 2, column = 3)
		self.obstacleSwitch = IntVar()
		self.obstacleSwitchCheckBox = Checkbutton(self.root, text="Add Obstacles", variable=self.obstacleSwitch).grid(row=3, column = 3)
		self.creatureSwitch = IntVar()
		self.creatureSwitchCheckBox = Checkbutton(self.root, text="Creature", variable=self.creatureSwitch).grid(row=2,column=4)
		self.cloudSwitch = IntVar()
		self.cloudSwitchCheckBox = Checkbutton(self.root, text="Cloud", variable=self.cloudSwitch).grid(row=1,column=4)
	def start(self):
		self.after(30, self.update_model)
	def restart(self):
		self.reset_ball_position()
		self.p1scores=0
		self.p2scores=0
		self.reset_panel()
	def reset_panel(self):
		self.canvas.delete(self.label)
		self.label = self.canvas.create_text(350,380, text=str(self.p1scores)+" | "+str(self.p2scores), fill='white')
		self.canvas.delete(self.speedLevel)			
		self.speedLevel = Scale(self.root, from_=1, to=4,orient = HORIZONTAL,resolution=0.5,command = self.paceChange)
		self.speedLevel.grid(row=2,column=1)
		self.currentlevel = 1
	def update_model(self):
		self.user1input = self.control1._user_input(self.canvas.coords(self.pad1),  self.buttons)
		self.user2input = self.control2._user_input(self.canvas.coords(self.pad2),  self.buttons)
		self.update_view()
	def update_view(self):
		if self.user1input == 1: self.canvas.move(self.pad1, 0 ,-self.control1.speed)
		if self.user1input == 2: self.canvas.move(self.pad1, 0 , self.control1.speed)
		if self.user2input == 1: self.canvas.move(self.pad2, 0 ,-self.control2.speed)
		if self.user2input == 2: self.canvas.move(self.pad2, 0 , self.control1.speed)
		self.after(10, self.update_model)
	def padSpeedChange(self,newSpeed):
		self.control1.speed = int(newSpeed)
		self.control2.speed = int(newSpeed)
def main():
    root = Tk()
    ui = UI(root)
    root.geometry("700x550")
    root.mainloop()  
if __name__ == '__main__':
    main()