from Tkinter import *
import math as math
import time
import random as random
import socket
class Ball(object):
	counter = 0 
	initV = 2.0
	newt = 0
	oldt = 0
	def __init__(self,x0 = 345,y0=200):
		self.collide = CollidePhysics()
		self.radius = 5.0
		self.ball_position = [x0,y0]
		self.v = [self.initV,-self.initV] # list contains x and y speed
	def updateVelocity_UnderGravity(self):	
		self.v[1]=self.v[1]+0.08
	def detec_collide_wall(self, ballcoords):
	# bounce ball when it hit the ceiling and floor
		if (ballcoords[1] <= 0) or (ballcoords[1] >= 390):
			self.v[1] = -self.v[1]
	def detec_collide_recpaddle(self,ballcoords,pad1coords,pad2coords,currentlevel,newspeedLevel):	
	# detect collision with rectangular paddle
		self.oldt = self.newt
		if self.collide.recpaddle_detec(ballcoords, pad1coords) or self.collide.recpaddle_detec(ballcoords, pad2coords):
			self.newt = time.time()
			tinterval = self.newt - self.oldt
			if (tinterval >= 1):
				self.v[0] = -self.v[0]
				if currentlevel<=4:			
					self.counter+=1
					if (self.counter==5):
						self.counter = 0 
						newspeedLevel.set(newspeedLevel.get()+0.5)	
	def detec_collide_arcpaddle(self,ballcoords, pad1coords, pad2coords, ballRadius, padRadius,currentlevel,newspeedLevel):		
		self.oldt = self.newt
		if (self.collide.arcpaddle_detec(ballcoords,pad1coords,ballRadius, padRadius)):
			self.change_Ball_Velocity_When_Hit_Arc(pad1coords,ballcoords,currentlevel,newspeedLevel)
		if (self.collide.arcpaddle_detec(ballcoords, pad2coords,ballRadius, padRadius)):
			self.change_Ball_Velocity_When_Hit_Arc(pad2coords,ballcoords,currentlevel,newspeedLevel)
	def change_Ball_Velocity_When_Hit_Arc(self,padcoords,ballcoords,currentlevel,newspeedLevel):
			self.newt = time.time()
			tinterval = self.newt - self.oldt
			if (tinterval >= 1):
					px = (padcoords[0]+padcoords[2])*0.5 ; py = (padcoords[1]+padcoords[3])*0.5 
					bx = (ballcoords[0]+ballcoords[2])*0.5 ; by = (ballcoords[1]+ballcoords[3])*0.5
					n = [ bx-px,by-py ]
					n_moduli = math.sqrt(n[0]*n[0]+n[1]*n[1])
					 # normalizing vectors
					n[0] = n[0]/n_moduli ; n[1] = n[1]/n_moduli
					v_moduli = math.sqrt(self.v[0]*self.v[0]+self.v[1]*self.v[1])
					self.v[0] = n[0]*v_moduli ; self.v[1] = n[1]*v_moduli
					if currentlevel<=4:			
						self.counter+=1
						if (self.counter==5):
							self.counter = 0 
							newspeedLevel.set(newspeedLevel.get()+0.5)
	def detec_score(self,ballcoords):
		if (ballcoords[0] <= 0):
			self.reset( )
			return 2
		if (ballcoords[2] >= 700 ):
			self.reset( )
			return 1
	def reset(self):
			self.v = [self.initV,-self.initV]
			self.counter = 0
class Paddle(object):
	def __init__(self,x0 = 5, y0 =4):
		self.position = [x0,y0]
		self.width = 10
		self.height = 50
class Controller(object):
	def __init__(self,up,down):
		self.up_key = up 
		self.down_key = down
		self.speed = 5
	def _user_input(self, coords, pressed):
		if self.up_key in pressed and self.down_key not in pressed:
			if coords[1]>=0:
				return 1
		if self.down_key in pressed and self.up_key not in pressed:
	 		if coords[3]<=400:	
				return 2
class Portals(object):
	start = 0 ; life = 0; oldt = 0 ; newt = 0
	def __init__(self):
		self.collide = CollidePhysics()
		self.portalLength = 100
		self.portalThickness = 5
		self.radius = 5
		self.target_coords = [0,0,0,0]			
	def generatePortalLocations(self):
		self.portal1pos = [random.randint(200,500), 5] #coords for top left corner of portal 1
		self.portal2pos = [random.randint(200,500), 395]
		self.setLife()
	def if_portal_life_cycle_is_over(self):
		if ( abs(time.time() - (self.start + self.life))<0.5 ):
			return 1
	def teleport_detector(self, ballcoords, portal1_coords, portal2_coords): 
		if self.collide.recpaddle_detec(ballcoords,portal1_coords):
			self.newt = time.time()
			if self.newt - self.oldt >0.5:
				self.target_coords[0] = portal2_coords[0]+self.portalLength/2-2*self.radius
				self.target_coords[1] = portal2_coords[1]-2*self.radius
				self.target_coords[2] = portal2_coords[2]-self.portalLength/2
				self.target_coords[3] = portal2_coords[3]-self.portalThickness
				self.oldt = self.newt
				return 1
		elif self.collide.recpaddle_detec(ballcoords,portal2_coords):
			self.newt = time.time()
			if self.newt - self.oldt >0.5:
				self.target_coords[0] = portal1_coords[0]+self.portalLength/2-2*self.radius
				self.target_coords[1] = portal1_coords[1]-2*self.radius
				self.target_coords[2] = portal1_coords[2]-self.portalLength/2
				self.target_coords[3] = portal1_coords[3]-self.portalThickness
				self.oldt = self.newt
				return 1
	def setLife(self):
		self.life = random.randint(4,6)
		self.start = time.time()
class Obstacle(object):	
	oldt1 = 0 ; newt1 = 0 ; oldt2 = 0 ; newt2 = 0; cv = [2,2]
	def __init__(self,Type):
		self.collideDetector = CollidePhysics()
		self.reborn=0
		self.type = Type
	def generate_Obs(self):
		if self.type == 'rectangle':
			self.initPos = [random.randint(200,500), random.randint(50,350)]
			self.width = random.randint(10,30) ; self.height = random.randint(60,80);	
			self.coords = self.initPos[0], self.initPos[1], self.initPos[0]+self.width, self.initPos[1]+self.height
			self.setLife()
		if self.type == 'oval':
			self.initPos = [random.randint(200,500), random.randint(50,350)]
			self.radii = random.randint(30,50);
			self.coords = self.initPos[0], self.initPos[1], self.initPos[0]+self.radii, self.initPos[1]+self.radii
			self.setLife()
		if self.type == 'cloud':
			self.initPos = [350, 200]
			self.cloud_detec_length = 180
			self.cloud_detec_width = 80
			self.coords = self.initPos[0]-90, self.initPos[1]-40,self.initPos[0]+self.cloud_detec_length-90, self.initPos[1]+self.cloud_detec_width-40
		if self.type == 'creature':
			self.initPos = [random.randint(200,400), random.randint(200,400)]
			radius = 30
			self.coords = self.initPos[0]-radius, self.initPos[1]-radius,self.initPos[0]+radius, self.initPos[1]+radius
			self.v = [1.5,-1.5]
	def if_creature_run_into_wall(self):
		self.oldt1 = self.newt1
		self.oldt2 = self.newt2
		if self.coords[0]<=0  or self.coords[2]>=700:
			self.newt1 = time.time()
			tinterval = self.newt1 - self.oldt1
			if tinterval>0.05:
				self.v[0] = -self.v[0]
		if (self.coords[1]<=0)  or (self.coords[3]>=400):
			self.newt2 = time.time()
			tinterval = self.newt2 - self.oldt2
			if tinterval>0.05:
				self.v[1] = -self.v[1]
	def _life_cycle_is_over(self):
		if ( abs(time.time() - (self.start + self.life))<0.5 ):
			return 1
		else:
			return 0
	def setLife(self):
		self.life = random.randint(4,7)
		self.start = time.time()
	def detec_collision(self,ballcoords,obscoords,ballV):	
			c= obscoords	
			if self.type == 'rectangle':	
				if self.collideDetector.recpaddle_detec(ballcoords, c):
					ballcenterx = 0.5*(ballcoords[0]+ballcoords[2]) 
					ballcentery = 0.5*(ballcoords[1]+ballcoords[3]) 
					if (ballcenterx < obscoords[0] ) or (ballcenterx > obscoords[2]):
						ballV[0] = -ballV[0] 
					if (ballcentery < obscoords[1] ) or (ballcentery > obscoords[3]):
						ballV[1] = -ballV[1]
					return 1
			if self.type == 'oval':
				if self.collideDetector.arcpaddle_detec(ballcoords, c,5, (c[2]-c[0])/2.5):
					obsx = (obscoords[0]+obscoords[2])*0.5 ; obsy = (obscoords[1]+obscoords[3])*0.5 
					bx = (ballcoords[0]+ballcoords[2])*0.5 ; by = (ballcoords[1]+ballcoords[3])*0.5
					n = [ bx-obsx,by-obsy ]
					n_moduli = math.sqrt(n[0]*n[0]+n[1]*n[1])
					n[0] = n[0]/n_moduli ; n[1] = n[1]/n_moduli
					v_moduli = math.sqrt(ballV[0]*ballV[0]+ballV[1]*ballV[1])
					ballV[0] = n[0]*v_moduli ; ballV[1] = n[1]*v_moduli
					return 1
			if self.type == 'cloud':
				self.oldt1 = self.newt1
				if self.collideDetector.cloud_detec(ballcoords,c):
					self.newt1 = time.time()
					tinterval = self.newt1 - self.oldt1
					if (tinterval >= 1):
						modulli=math.sqrt(ballV[0]*ballV[0]+ballV[1]*ballV[1])
						theta = math.radians(random.random()*360)
						ballV[0]=modulli*math.cos(theta) 
						ballV[1]=modulli*math.sin(theta)
			if self.type == 'creature':
				if self.collideDetector.arcpaddle_detec(ballcoords, obscoords,5, (obscoords[2]-obscoords[0])/2):
					obsx = (obscoords[0]+obscoords[2])*0.5 ; obsy = (obscoords[1]+obscoords[3])*0.5 
					bx = (ballcoords[0]+ballcoords[2])*0.5 ; by = (ballcoords[1]+ballcoords[3])*0.5
					n = [ bx-obsx,by-obsy ]
					n_moduli = math.sqrt(n[0]*n[0]+n[1]*n[1])
					n[0] = n[0]/n_moduli ; n[1] = n[1]/n_moduli
					v_moduli = math.sqrt(ballV[0]*ballV[0]+ballV[1]*ballV[1])
					ballV[0] = n[0]*v_moduli ; ballV[1] = n[1]*v_moduli
class CollidePhysics(object):
	def recpaddle_detec(self, coords1, coords2): # coords1 = ball_coords, coords2 = paddle_coordsprint
		return (coords1[2] >= coords2[0] and coords1[3] >= coords2[1] and coords1[0] <= coords2[2] and coords1[1] <= coords2[3])		
	def arcpaddle_detec(self,ballCoords,padCoords,ballRadius, padRadius):
		padCenterx = (padCoords[0]+padCoords[2])*0.5;
		padCentery = (padCoords[1]+padCoords[3])*0.5;
		ballCenterx = (ballCoords[0]+ballCoords[2])*0.5;
		ballCentery = (ballCoords[1]+ballCoords[3])*0.5;
		distance = math.sqrt(pow(padCenterx-ballCenterx,2)+pow(padCentery-ballCentery,2));
		if abs(distance-(ballRadius+padRadius))<=4:
			return True;
	def cloud_detec(self, ballCoords, cloudcoords):
		ballcenterx = (ballCoords[0]+ballCoords[2])*0.5; cloudcenterx = (cloudcoords[0]+cloudcoords[2])*0.5
		ballcentery = (ballCoords[1]+ballCoords[3])*0.5; cloudcentery = (cloudcoords[1]+cloudcoords[3])*0.5
		a = (cloudcoords[2]-cloudcoords[0]) * 0.5 
		b = (cloudcoords[3]-cloudcoords[1]) * 0.5
		if ( pow(ballcenterx-cloudcenterx,2)/pow(a,2)+pow(ballcentery-cloudcentery,2)/pow(b,2)-1 < -0 ):
			return True
class UI(Frame):
	teleport_detected = 0
	winHeight = 400
	winWidth = 700
	p1scores = 0 
	p2scores = 0
	currentlevel = 1
	gravity=0
	obstaclesOn = 0
	portalsOn = 0
	cloudOn = 0
	creatureOn = 0
	timecheckpoint = time.time()
	obsStatusList = [0] * 5
	numberOfObs = 6
	obsObjList = []
	obsNameList = ['rectangle', 'oval']
	obsList = [ ]

	def __init__(self,parent):
		Frame.__init__(self,parent)
		self.root = parent	
		self.collide = CollidePhysics()
		self.root.title("PONG")
		self.canvas = Canvas(self.root,bg='black')
		self.canvas.configure(width = self.winWidth, height=self.winHeight)
		self.canvas.create_line(self.winWidth/2,0,self.winWidth/2,self.winHeight,fill = 'yellow')
		self.canvas.grid(row=0, column = 0,columnspan=10)
		self.label = self.canvas.create_text(self.winWidth/2,self.winHeight-20, text=str(self.p1scores)+" | "+str(self.p2scores), fill='white')		
		self.ballObj = Ball()
		self.pad1Obj = Paddle()
		self.pad2Obj = Paddle(687)
		self.obs1Obj = Obstacle('rectangle')
		self.obs2Obj = Obstacle('rectangle')
		self.obs3Obj = Obstacle('oval')
		self.obs4Obj = Obstacle('oval')
		self.cloudObj = Obstacle('cloud')
		self.im = PhotoImage(file = "cloud1.gif")
		self.creatureObj = Obstacle('creature')
		self.cim = PhotoImage(file = "original.gif")
		self.portalsObj = Portals()
		self.ball = self.canvas.create_oval(self.ballObj.ball_position[0]+0, 
		 				self.ballObj.ball_position[1]+0, 2*self.ballObj.radius+self.ballObj.ball_position[0], 
		 				2*self.ballObj.radius+self.ballObj.ball_position[1], outline = 'white', fill = 'white', width = 1)
		self.pad1 = self.canvas.create_rectangle(0+self.pad1Obj.position[0], 0+self.pad1Obj.position[1], 
						self.pad1Obj.width+self.pad1Obj.position[0], 
	 					self.pad1Obj.height+self.pad1Obj.position[1], outline="white", fill="white")
	 	self.pad2 = self.canvas.create_rectangle(0+self.pad2Obj.position[0], 0+self.pad2Obj.position[1], 
						self.pad2Obj.width+self.pad2Obj.position[0], 
	 					self.pad2Obj.height+self.pad2Obj.position[1], outline="white", fill="white")
	 	self.control1 = Controller('w','s')
	 	self.control2 = Controller('Up','Down')
	 	self.buttons = set()
		self.root.bind_all('<KeyPress>', lambda event: self.buttons.add(event.keysym))
		self.root.bind_all('<KeyRelease>', lambda event: self.buttons.discard(event.keysym))		
	 	Button(self.root,text="START", command=self.start).grid(row=1,column=0)
	 	Button(self.root,text="QUIT", command=self.root.destroy).grid(row=2,column=0)
		Button(self.root,text="RESET", command=self.restart).grid(row=3,column=0)		
		# ball speed adjustment
		Label(self.root, text="Ball Speed Control").grid(row=1,column=1)
		self.speedLevel = Scale(self.root, from_=1, to=4,orient = HORIZONTAL,resolution=0.5,command = self.paceChange)
		self.speedLevel.grid(row=2,column=1)
		self.speedLevel.set(1)
		# pad speed adjustment
		Label(self.root, text="Paddle Speed Control").grid(row=1,column=2)
		self.padSpeedLevel = Scale(self.root, from_=5, to=15,orient = HORIZONTAL,resolution=1,command = self.padSpeedChange).grid(row=2,column=2)
		self.pad_shape = IntVar()
		self.PaddleShapeCheckBox = Checkbutton(self.root, text="Tricky Paddle", variable=self.pad_shape, command=self.changePaddleShape).grid(row=3,column=2)	
		self.life = random.randint(8,12)
	def start(self):
		self.after(30, self.update_model)
	def restart(self):
		self.reset_ball_position()
		self.p1scores=0
		self.p2scores=0
		self.reset_panel()
	def reset_panel(self):
		self.canvas.delete(self.label)
		self.label = self.canvas.create_text(350,380, text=str(self.p1scores)+" | "+str(self.p2scores), fill='white')
		self.canvas.delete(self.speedLevel)			
		self.speedLevel = Scale(self.root, from_=1, to=4,orient = HORIZONTAL,resolution=0.5,command = self.paceChange)
		self.speedLevel.grid(row=2,column=1)
		self.currentlevel = 1
	def update_model(self):

		self.ballObj.detec_collide_wall(self.canvas.coords(self.ball))
		self.user1input = self.control1._user_input(self.canvas.coords(self.pad1),  self.buttons)
		self.user2input = self.control2._user_input(self.canvas.coords(self.pad2),  self.buttons)
		self.whoscore = self.ballObj.detec_score(self.canvas.coords(self.ball))
		self.timer(self.changeoObsStatus)

		if self.pad_shape.get() == 0:
		 	self.ballObj.detec_collide_recpaddle(self.canvas.coords(self.ball),self.canvas.coords(self.pad1),self.canvas.coords(self.pad2), self.currentlevel, self.speedLevel)
		if self.pad_shape.get() == 1:
		 	self.ballObj.detec_collide_arcpaddle(self.canvas.coords(self.ball), 
		 											self.canvas.coords(self.pad1), 
		 												self.canvas.coords(self.pad2), 
		 													self.ballObj.radius,
		 														self.pad1Obj.height/2,
		 															self.currentlevel,
		 																self.speedLevel)

		if self.obsStatusList[0]==1:
		 	self.ballObj.updateVelocity_UnderGravity( )
		if self.obsStatusList[1]==1 and self.portalsOn == 1:
			if self.portalsObj.teleport_detector(self.canvas.coords(self.ball), self.canvas.coords(self.portal1),self.canvas.coords(self.portal2) ):
				self.teleport_detected = 1
		if self.obsStatusList[4] == 1 and self.creatureOn == 1:
				self.creatureObj.if_creature_run_into_wall()
				self.creatureObj.detec_collision(self.canvas.coords(self.ball),self.canvas.coords(self.creature),self.ballObj.v)
		if self.obsStatusList[3]==1 and self.cloudOn ==1 :
				self.cloudObj.detec_collision( self.canvas.coords(self.ball), self.canvas.coords(self.cloud), self.ballObj.v)
				self.cloudOn =1
		if self.obsStatusList[2]==1 and self.obstaclesOn == 0:
			self.generateObsObjs()
		if self.obsStatusList[2]==0 and self.obstaclesOn == 1:
			self.obsObjList = []
		if self.obsStatusList[2] and self.obstaclesOn == 1:
			for i in range (0, self.numberOfObs):
				if self.obsObjList[i]._life_cycle_is_over() or self.obsObjList[i].detec_collision(self.canvas.coords(self.ball), self.obsObjList[i].coords,self.ballObj.v): 
					self.obsObjList[i].generate_Obs()
					self.obsObjList[i].reborn = 1

		self.update_view()
	def update_view(self):
		if self.obsStatusList[1]==1 and self.portalsOn == 0:
			self.draw_portals()
			self.portalsOn = 1 
		if self.obsStatusList[1] and self.portalsOn == 1:
			if self.teleport_detected == 1:
				self.canvas.coords(self.ball, self.portalsObj.target_coords[0],self.portalsObj.target_coords[1],
									self.portalsObj.target_coords[2],self.portalsObj.target_coords[3])
				self.teleport_detected = 0
			if self.portalsObj.if_portal_life_cycle_is_over():
				self.delete_portals()
				self.draw_portals()
		if self.obsStatusList[1]==0 and self.portalsOn == 1:
			self.delete_portals()
			self.portalsOn = 0


		if self.obsStatusList[2]==1 and self.obstaclesOn == 0:

			for i in range(0,self.numberOfObs):
				self.obsList.append(self.draw_obstacle(self.obsObjList[i]))
				self.obstaclesOn = 1
		if self.obsStatusList[2]==0 and self.obstaclesOn == 1:
			for i in range(0,self.numberOfObs):
				self.delete_obstacle(self.obsList[i])
			self.obsList = []
			self.obstaclesOn = 0
		if self.obsStatusList[2]==1 and self.obstaclesOn == 1:
			for i in range(0,self.numberOfObs):
				if self.obsObjList[i].reborn == 1:
					self.delete_obstacle(self.obsList[i])
					self.obsList[i] = self.draw_obstacle(self.obsObjList[i])
					self.obsObjList[i].reborn = 0
		if self.obsStatusList[3]==1 and self.cloudOn ==0 :
			self.draw_cloud( )
			self.cloudOn =1
		if self.obsStatusList[3]==0 and self.cloudOn ==1 :
			self.delete_cloud( )
			self.cloudOn =0
		if self.obsStatusList[4]==1 and self.creatureOn ==0 :
			self.draw_creature( )
			self.creatureOn =1
		if self.obsStatusList[4]==1 and self.creatureOn ==1 :
			self.move_creature()	
		if self.obsStatusList[4]==0 and self.creatureOn==1 :
			self.delete_creature( )
			self.creatureOn =0
		self.canvas.move(self.ball, self.ballObj.v[0],self.ballObj.v[1])
		if self.user1input == 1: self.canvas.move(self.pad1, 0 ,-self.control1.speed)
		if self.user1input == 2: self.canvas.move(self.pad1, 0 , self.control1.speed)
		if self.user2input == 1: self.canvas.move(self.pad2, 0 ,-self.control2.speed)
		if self.user2input == 2: self.canvas.move(self.pad2, 0 , self.control1.speed)
		if self.whoscore==1:
			self.p1scores+=1
			self.reset_panel()
			self.reset_ball_position()
		if self.whoscore==2:
		 	self.p2scores+=1
		 	self.reset_panel()
		 	self.reset_ball_position()
		self.after(10, self.update_model)
	def reset_ball_position(self):
		self.after(200)
		self.canvas.coords(self.ball, self.ballObj.ball_position[0]+0, 
		 				self.ballObj.ball_position[1]+0, 2*self.ballObj.radius+self.ballObj.ball_position[0], 
		 				2*self.ballObj.radius+self.ballObj.ball_position[1])
	def paceChange(self,new):
		new = float(new)
		self.ballObj.v[0]= self.ballObj.v[0]/self.currentlevel*new
		self.ballObj.v[1]= self.ballObj.v[1]/self.currentlevel*new
		self.currentlevel = new
	def padSpeedChange(self,newSpeed):
		self.control1.speed = int(newSpeed)
		self.control2.speed = int(newSpeed)
	def draw_arcpad(self, pad):
		coords = self.canvas.coords(pad)
		if coords[0]<350:
	 		self.canvas.delete(pad)
	 		coords = coords[0]-self.pad1Obj.height/2, coords[1],coords[0]+self.pad1Obj.height/2,coords[3] 
	 		return self.canvas.create_arc(coords, start = -90, extent=180, outline="white", fill="white")
	 	else:
	 		self.canvas.delete(pad)
	 		coords = coords[2]+self.pad2Obj.height/2, coords[1],coords[2]-self.pad2Obj.height/2,coords[3] 
			return (self.canvas.create_arc(coords, start = 90, extent=180, outline="white", fill="white"))
	def redraw_recpad(self,pad):
		coords = self.canvas.coords(pad)
		if coords[0]<350:
			self.canvas.delete(pad)
	 		centerx = (coords[0]+coords[2])*0.5 
	 		coords = centerx, coords[1],centerx+self.pad1Obj.width,coords[3]
			return self.canvas.create_rectangle(coords, outline="white",fill="white")
		else:
			self.canvas.delete(pad)
	 		centerx = (coords[0]+coords[2])*0.5 
			coords = centerx-self.pad1Obj.width, coords[1],centerx,coords[3]
			return self.canvas.create_rectangle(coords, outline="white",fill="white")
	def changePaddleShape(self):
		if self.pad_shape.get()==0:		
			self.pad1 = self.redraw_recpad(self.pad1)
			self.pad2 = self.redraw_recpad(self.pad2)
		if self.pad_shape.get()==1:			
			self.pad1 = self.draw_arcpad(self.pad1)
			self.pad2 = self.draw_arcpad(self.pad2)
	def draw_portals(self):
		self.portalsObj.generatePortalLocations()
		self.portal1 = self.canvas.create_oval(self.portalsObj.portal1pos[0], self.portalsObj.portal1pos[1], self.portalsObj.portal1pos[0]+self.portalsObj.portalLength, 
			 				self.portalsObj.portal1pos[1]+self.portalsObj.portalThickness, outline = 'purple')		
		self.portal2 = self.canvas.create_oval(self.portalsObj.portal2pos[0], self.portalsObj.portal2pos[1], self.portalsObj.portal2pos[0]+self.portalsObj.portalLength, 
			 				self.portalsObj.portal2pos[1]+self.portalsObj.portalThickness, outline = 'purple')
	def delete_portals(self):
		self.canvas.delete(self.portal1)
		self.canvas.delete(self.portal2)
	def draw_obstacle(self, obsObj):
		if obsObj.type == 'rectangle':
			return (self.canvas.create_rectangle(obsObj.coords[0],obsObj.coords[1],obsObj.coords[2],
									obsObj.coords[3],outline = 'blue', fill = ''))
		if obsObj.type == 'oval':
			return (self.canvas.create_oval(obsObj.coords[0],obsObj.coords[1],obsObj.coords[2],
									obsObj.coords[3],outline = 'orange', fill = '') )
	def changeoObsStatus(self):
		for i in range(0, len(self.obsStatusList)):
			self.obsStatusList[i] = random.randint(0,1)
	def timer(self,func):
		if abs(time.time()-(self.life+self.timecheckpoint))<0.3:
			self.timecheckpoint = time.time()
			self.life = random.randint(8,12)
			func()
	def delete_obstacle(self,obs):
		self.canvas.delete(obs)
	def generateObsObjs(self):
		for i in range(0,self.numberOfObs):
			self.obsObjList.append(Obstacle(self.obsNameList[random.randint(0,1)])) 
			self.obsObjList[i].generate_Obs()
	def draw_cloud(self):	
		self.cloudObj.generate_Obs()	
		self.cloud = self.canvas.create_oval(self.cloudObj.coords[0], self.cloudObj.coords[1]+10, self.cloudObj.coords[2], self.cloudObj.coords[3]+5,fill = '', outline='')
		self.tk_im = self.canvas.create_image(self.cloudObj.initPos[0],self.cloudObj.initPos[1],image=self.im)
	def delete_cloud(self):
		self.canvas.delete(self.cloud)
		self.canvas.delete(self.tk_im)
	def draw_creature(self):
		self.creatureObj.generate_Obs()
		self.creature = self.canvas.create_oval(self.creatureObj.coords[0], self.creatureObj.coords[1], self.creatureObj.coords[2], self.creatureObj.coords[3],fill = '', outline='')
		self.tk_cim = self.canvas.create_image(self.creatureObj.initPos[0],self.creatureObj.initPos[1],image=self.cim)
	def delete_creature(self):
		self.canvas.delete(self.creature)
		self.canvas.delete(self.tk_cim)
	def move_creature(self):
		self.canvas.delete(self.tk_cim)
		self.canvas.move( self.creature,self.creatureObj.v[0],self.creatureObj.v[1]) 
		self.creatureObj.coords = self.canvas.coords(self.creature)
		self.tk_cim = self.canvas.create_image( (self.creatureObj.coords[0]+self.creatureObj.coords[2])/2.0 ,(self.creatureObj.coords[1]+self.creatureObj.coords[3])/2.0,image=self.cim)
def main():
    root = Tk()
    ui = UI(root)
    root.geometry("700x550")
    root.mainloop()  
if __name__ == '__main__':
    main()