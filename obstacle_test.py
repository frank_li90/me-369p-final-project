import math as math
import time
import random as random

class Obstacle(object):	
	cv = [2,2]
	def __init__(self,Type):
		self.collideDetector = CollidePhysics()
		self.type = Type
	def generate_Obs(self):
		if self.type == 'rectangle':
			self.initPos = [random.randint(200,500), random.randint(50,350)]
			self.width = random.randint(10,30) ; self.height = random.randint(60,80);	
			self.coords = self.initPos[0], self.initPos[1], self.initPos[0]+self.width, self.initPos[1]+self.height
			self.setLife()
		if self.type == 'oval':
			self.initPos = [random.randint(200,500), random.randint(50,350)]
			self.radii = random.randint(30,50);
			self.coords = self.initPos[0], self.initPos[1], self.initPos[0]+self.radii, self.initPos[1]+self.radii
			self.setLife()
		if self.type == 'cloud':
			self.initPos = [350, 200]
			self.cloud_detec_length = 180
			self.cloud_detec_width = 80
			self.coords = self.initPos[0]-90, self.initPos[1]-40,self.initPos[0]+self.cloud_detec_length-90, self.initPos[1]+self.cloud_detec_width-40
		if self.type == 'creature':
			self.initPos = [random.randint(200,400), random.randint(200,400)]
			radius = 30
			self.coords = self.initPos[0]-radius, self.initPos[1]-radius,self.initPos[0]+radius, self.initPos[1]+radius
			self.v = [1.5,-1.5]

	def detec_collision(self,ballcoords,obscoords,ballV):	
			c= obscoords	
			if self.type == 'rectangle':	
				if self.collideDetector.recpaddle_detec(ballcoords, c):
					ballcenterx = 0.5*(ballcoords[0]+ballcoords[2]) 
					ballcentery = 0.5*(ballcoords[1]+ballcoords[3]) 
					if (ballcenterx < obscoords[0] ) or (ballcenterx > obscoords[2]):
						ballV[0] = -ballV[0] 
					if (ballcentery < obscoords[1] ) or (ballcentery > obscoords[3]):
						ballV[1] = -ballV[1]
					return 1
			if self.type == 'oval':
				if self.collideDetector.arcpaddle_detec(ballcoords, c,5, (c[2]-c[0])/2.5):
					obsx = (obscoords[0]+obscoords[2])*0.5 ; obsy = (obscoords[1]+obscoords[3])*0.5 
					bx = (ballcoords[0]+ballcoords[2])*0.5 ; by = (ballcoords[1]+ballcoords[3])*0.5
					n = [ bx-obsx,by-obsy ]
					n_moduli = math.sqrt(n[0]*n[0]+n[1]*n[1])
					n[0] = n[0]/n_moduli ; n[1] = n[1]/n_moduli
					v_moduli = math.sqrt(ballV[0]*ballV[0]+ballV[1]*ballV[1])
					ballV[0] = n[0]*v_moduli ; ballV[1] = n[1]*v_moduli
					return 1
			if self.type == 'cloud':
				self.oldt1 = self.newt1
				if self.collideDetector.cloud_detec(ballcoords,c):
					self.newt1 = time.time()
					tinterval = self.newt1 - self.oldt1
					if (tinterval >= 1):
						modulli=math.sqrt(ballV[0]*ballV[0]+ballV[1]*ballV[1])
						theta = math.radians(random.random()*360)
						ballV[0]=modulli*math.cos(theta) 
						ballV[1]=modulli*math.sin(theta)
				return 1
			if self.type == 'creature':
				if self.collideDetector.arcpaddle_detec(ballcoords, obscoords,5, (obscoords[2]-obscoords[0])/2):
					obsx = (obscoords[0]+obscoords[2])*0.5 ; obsy = (obscoords[1]+obscoords[3])*0.5 
					bx = (ballcoords[0]+ballcoords[2])*0.5 ; by = (ballcoords[1]+ballcoords[3])*0.5
					n = [ bx-obsx,by-obsy ]
					n_moduli = math.sqrt(n[0]*n[0]+n[1]*n[1])
					n[0] = n[0]/n_moduli ; n[1] = n[1]/n_moduli
					v_moduli = math.sqrt(ballV[0]*ballV[0]+ballV[1]*ballV[1])
					ballV[0] = n[0]*v_moduli ; ballV[1] = n[1]*v_moduli

					return

class Portals(object):
	start = 0 ; life = 0; oldt = 0 ; newt = 0
	def __init__(self):
		self.collide = CollidePhysics()
		self.portalLength = 100
		self.portalThickness = 5
		self.radius = 5
		self.target_coords = [0,0,0,0]			
	def generatePortalLocations(self):
		self.portal1pos = [random.randint(200,500), 5] #coords for top left corner of portal 1
		self.portal2pos = [random.randint(200,500), 395]
		self.setLife()
	def if_portal_life_cycle_is_over(self):
		if ( abs(time.time() - (self.start + self.life))<0.5 ):
			return 1
	def teleport_detector(self, ballcoords, portal1_coords, portal2_coords): 
		if self.collide.recpaddle_detec(ballcoords,portal1_coords):
			self.newt = time.time()
			if self.newt - self.oldt >0.5:
				self.target_coords[0] = portal2_coords[0]+self.portalLength/2-2*self.radius
				self.target_coords[1] = portal2_coords[1]-2*self.radius
				self.target_coords[2] = portal2_coords[2]-self.portalLength/2
				self.target_coords[3] = portal2_coords[3]-self.portalThickness
				self.oldt = self.newt
				return 1
		elif self.collide.recpaddle_detec(ballcoords,portal2_coords):
			self.newt = time.time()
			if self.newt - self.oldt >0.5:
				self.target_coords[0] = portal1_coords[0]+self.portalLength/2-2*self.radius
				self.target_coords[1] = portal1_coords[1]-2*self.radius
				self.target_coords[2] = portal1_coords[2]-self.portalLength/2
				self.target_coords[3] = portal1_coords[3]-self.portalThickness
				self.oldt = self.newt
				return 1
	def setLife(self):
		self.life = random.randint(4,6)
		self.start = time.time()
class Ball(object):
	counter = 0 
	initV = 2.0
	newt = 0
	oldt = 0
	def __init__(self,x0 = 345,y0=200):
		self.collide = CollidePhysics()
		self.radius = 5.0
		self.position = [x0,y0]
		self.v = [self.initV,-self.initV] # list contains x and y speed
	def updateVelocity_UnderGravity(self):	
		self.v[1]=self.v[1]+0.08
	def detec_collide_wall(self, ballcoords):
	# bounce ball when it hit the ceiling and floor
		if (ballcoords[1] <= 0) or (ballcoords[1] >= 390):
			self.v[1] = -self.v[1]
	def detec_collide_recpaddle(self,ballcoords,pad1coords,pad2coords,currentlevel,newspeedLevel):	
	# detect collision with rectangular paddle
		self.oldt = self.newt
		if self.collide.recpaddle_detec(ballcoords, pad1coords) or self.collide.recpaddle_detec(ballcoords, pad2coords):
			self.newt = time.time()
			tinterval = self.newt - self.oldt
			if (tinterval >= 1):
				self.v[0] = -self.v[0]
				if currentlevel<=4:			
					self.counter+=1
					if (self.counter==5):
						self.counter = 0 
						newspeedLevel.set(newspeedLevel.get()+0.5)	
	def detec_collide_arcpaddle(self,ballcoords, pad1coords, pad2coords, ballRadius, padRadius,currentlevel,newspeedLevel):		
		self.oldt = self.newt
		if (self.collide.arcpaddle_detec(ballcoords,pad1coords,ballRadius, padRadius)):
			self.change_Ball_Velocity_When_Hit_Arc(pad1coords,ballcoords,currentlevel,newspeedLevel)
		if (self.collide.arcpaddle_detec(ballcoords, pad2coords,ballRadius, padRadius)):
			self.change_Ball_Velocity_When_Hit_Arc(pad2coords,ballcoords,currentlevel,newspeedLevel)
	def change_Ball_Velocity_When_Hit_Arc(self,padcoords,ballcoords,currentlevel,newspeedLevel):
			self.newt = time.time()
			tinterval = self.newt - self.oldt
			if (tinterval >= 1):
					px = (padcoords[0]+padcoords[2])*0.5 ; py = (padcoords[1]+padcoords[3])*0.5 
					bx = (ballcoords[0]+ballcoords[2])*0.5 ; by = (ballcoords[1]+ballcoords[3])*0.5
					n = [ bx-px,by-py ]
					n_moduli = math.sqrt(n[0]*n[0]+n[1]*n[1])
					 # normalizing vectors
					n[0] = n[0]/n_moduli ; n[1] = n[1]/n_moduli
					v_moduli = math.sqrt(self.v[0]*self.v[0]+self.v[1]*self.v[1])
					self.v[0] = n[0]*v_moduli ; self.v[1] = n[1]*v_moduli
					if currentlevel<=4:			
						self.counter+=1
						if (self.counter==5):
							self.counter = 0 
							newspeedLevel.set(newspeedLevel.get()+0.5)
	def detec_score(self,ballcoords):
		if (ballcoords[0] <= 0):
			self.reset( )
			return 2
		if (ballcoords[2] >= 700 ):
			self.reset( )
			return 1
	def reset(self):
			self.v = [self.initV,-self.initV]
			self.counter = 0
class CollidePhysics(object):
	def recpaddle_detec(self, coords1, coords2): # coords1 = ball_coords, coords2 = paddle_coordsprint
		return (coords1[2] >= coords2[0] and coords1[3] >= coords2[1] and coords1[0] <= coords2[2] and coords1[1] <= coords2[3])		
	def arcpaddle_detec(self,ballCoords,padCoords,ballRadius, padRadius):
		padCenterx = (padCoords[0]+padCoords[2])*0.5;
		padCentery = (padCoords[1]+padCoords[3])*0.5;
		ballCenterx = (ballCoords[0]+ballCoords[2])*0.5;
		ballCentery = (ballCoords[1]+ballCoords[3])*0.5;
		distance = math.sqrt(pow(padCenterx-ballCenterx,2)+pow(padCentery-ballCentery,2));
		if abs(distance-(ballRadius+padRadius))<=4:
			return True;
	def cloud_detec(self, ballCoords, cloudcoords):
		ballcenterx = (ballCoords[0]+ballCoords[2])*0.5; cloudcenterx = (cloudcoords[0]+cloudcoords[2])*0.5
		ballcentery = (ballCoords[1]+ballCoords[3])*0.5; cloudcentery = (cloudcoords[1]+cloudcoords[3])*0.5
		a = (cloudcoords[2]-cloudcoords[0]) * 0.5 
		b = (cloudcoords[3]-cloudcoords[1]) * 0.5
		if ( pow(ballcenterx-cloudcenterx,2)/pow(a,2)+pow(ballcentery-cloudcentery,2)/pow(b,2)-1 < -0 ):
			return True
class Paddle(object):
	def __init__(self,x0 = 5, y0 =4):
		self.position = [x0,y0]
		self.width = 10
		self.height = 50
		self.coords = ( self.position[0], self.position[1], self.position[0]+self.width, self.position[1]+self.height )

# testing basic parameters of the obstacles

obs1 = Obstacle('rectangle') ; obs1.generate_Obs()
obs2 = Obstacle('oval') ; obs2.generate_Obs()
obs3 = Obstacle('creature') ; obs3.generate_Obs()
obs4 = Obstacle('cloud') ; obs4.generate_Obs()

print obs1.type, obs1.coords
print obs2.type, obs2.coords
print obs3.type, obs3.coords, obs3.v
print obs4.type, obs4.coords

ball = Ball()
print ball.position, ball.v

pad = Paddle()
print pad.coords

# testing collision with paddles and obstacles

x = ( obs1.coords[1] + obs1.coords[3] )/2.0 
y = ( obs1.coords[0] + obs1.coords[2] )/2.0+10

ballcoords = x-ball.radius, y-ball.radius, x+ball.radius, y+ball.radius

ball.v = [-1,0]

while ( obs1.detec_collision(ballcoords, obs1.coords, ball.v)!=1) 
